//
//  VovanAppDelegate.h
//  FriendsList
//
//  Created by student on 12.09.14.
//  Copyright (c) 2014 Name. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VovanAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
