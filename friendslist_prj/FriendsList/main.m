//
//  main.m
//  FriendsList
//
//  Created by student on 12.09.14.
//  Copyright (c) 2014 Name. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "VovanAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([VovanAppDelegate class]));
    }
}
