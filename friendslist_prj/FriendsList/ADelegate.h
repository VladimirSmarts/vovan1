//
//  ADelegate.h
//  FriendsList
//
//  Created by student on 15.09.14.
//  Copyright (c) 2014 Name. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ADelegate <NSObject>

    -(void) run;

@end
