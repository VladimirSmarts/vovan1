//
//  AClass.m
//  FriendsList
//
//  Created by student on 15.09.14.
//  Copyright (c) 2014 Name. All rights reserved.
//

#import "AClass.h"

@implementation AClass

- (void)myMetod{
    
    NSLog(@"myMetod called");
    
    [self.delegate run];
    
}

@end

