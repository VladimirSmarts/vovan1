//
//  VovanViewController.m
//  FriendsList
//
//  Created by student on 12.09.14.
//  Copyright (c) 2014 Name. All rights reserved.
//

#import "VovanViewController.h"
#import "AClass.h"
#import "BClass.h"
#import "SecondViewController.h"

@interface VovanViewController ()

@property(nonatomic, strong)NSDictionary *friendsList;
- (IBAction)showModal:(id)sender;

@end

@implementation VovanViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
 /*
    NSArray* friendList = @[@"Влад Смирнов", @"Сергей Дятлов", @"Игорь Козлов", @"Дмитрий Мельник"];
    
    NSMutableDictionary* resultDict = [NSMutableDictionary dictionary];
    
    for (NSString* fullName in friendList) {
        
        NSString* lastName = [[fullName componentsSeparatedByString:@" "] lastObject];

        NSString* dictionaryKey = [lastName substringToIndex:1];
        
        if ([resultDict objectForKey:dictionaryKey]==nil) {
            
            [resultDict setObject:[NSMutableArray arrayWithObject:fullName] forKey:dictionaryKey];
            
        } else {
            
            NSMutableArray* arrayForKey = [resultDict objectForKey:dictionaryKey];
            [arrayForKey addObject:fullName];
            [resultDict setObject:arrayForKey forKey:dictionaryKey];
        }
        
    }

    self.friendsList = resultDict;
    
//    NSLog(@"%@", resultDict);

    
    
//    delegate
    AClass *a = [[AClass alloc] init];
    BClass *b = [[BClass alloc] init];
    a.delegate = b;
    [a myMetod];
  */
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)showModal:(id)sender
{
    SecondViewController *secondVC = [[SecondViewController alloc] init];
    secondVC.modalTransitionStyle = UIModalTransitionStylePartialCurl;
    [self presentViewController:secondVC animated:YES completion:nil];
    
}

#pragma mark - UITableViewDataSource

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return [[self.friendsList allKeys] count];
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSString *key = [[self.friendsList allKeys] objectAtIndex:section];
    NSMutableArray *array = [self.friendsList objectForKey:key];
    
    return [array count];
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    
    NSString *key = [[self.friendsList allKeys] objectAtIndex:section];
    
    return key;
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (cell==nil) {
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
        
    }
    //    cell.textLabel.text = [NSString stringWithFormat:@"Text %d", indexPath.row];
    
    NSArray * keys = [self.friendsList allKeys];
    NSString * key = [keys objectAtIndex:indexPath.section];
    NSMutableArray *objects = [self.friendsList objectForKey:key];
    NSString *fullName = [objects objectAtIndex:indexPath.row];
    cell.textLabel.text = fullName;

    return cell;

}


@end
