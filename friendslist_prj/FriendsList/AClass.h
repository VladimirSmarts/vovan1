//
//  AClass.h
//  FriendsList
//
//  Created by student on 15.09.14.
//  Copyright (c) 2014 Name. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ADelegate.h"

@interface AClass : NSObject

    @property(nonatomic, weak) id<ADelegate> delegate;

    -(void) myMetod;

@end
